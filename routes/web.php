<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/generate-users', function () {
    //this can be used to generate users
    $users = factory(MailChimp\User::class, 50)->create();
    echo "users generated";
});

Auth::routes();
Route::get('/lists/{list}/members/create', 'MembersController@create')->name('lists.members.create');
Route::get('/members/{member}/edit', 'MembersController@edit')->name('lists.members.edit');

Route::resource('/lists', 'MailListsController');

Route::post('lists/{list}/members/store', 'MembersController@store')->name('members.store');
Route::patch('/members/{member}/update', 'MembersController@update')->name('members.update');
Route::delete('/members/{member}/destroy', 'MembersController@destroy')->name('members.destroy');
