#Mail Chimp Integration

This Laravel App can manage Lists and Subscribers of Mailchimp

###Installation
```sh
$ composer install
$ php artisan migrate
```
###Settings
add your mailchimp API in .env file

- MAIL_CHIMP_KEY=xxxxxxxxxxxxxxxxx

generate users (this will be used as Contact for List)

- /generate-users route will generate 50 users
