@extends('layouts.master')
@section('content')
<div class="starter-template">
  <div class="row">
      <div class="col-12">
        @if (Session::has('message'))
                 <div class="alert {{Session::get('code')}} alert-dismissable fade show" role="alert">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                   </button>
                   <h4>{{ Session::get('message') }}</h4>
                 </div>
        @endif
      </div>
  </div>
  <div class="row">
        <div class="col-4"><strong>Name</strong></div>
        <div class="col-8">{{$record->name}}</div>
  </div>
  <div class="row">
        <div class="col-4"><strong>Contact</strong></div>
        <div class="col-8">{{$record->contact->name}}, {{$record->contact->company}}</div>
  </div>
  <div class="row">
        <div class="col-4"><strong>Subject</strong></div>
        <div class="col-8">{{$record->subject}}</div>
  </div>
  <div class="row">
        <div class="col-4"><strong>MailChimp Admin URL</strong></div>
        <div class="col-8"><a target='_blank' href="https://{{ MailChimp\Services\DisplayServices::fetchDataCenter()}}.admin.mailchimp.com/lists/members/?id={{ $record->mailchimp_webid}}">
          https://{{ MailChimp\Services\DisplayServices::fetchDataCenter()}}.admin.mailchimp.com/lists/members/?id={{ $record->mailchimp_webid}}
        </a></div>
  </div>
</div>
<div class="members" style="border-top:.1rem brown solid;margin-top:1rem;">
  <div class="row">
        <div class="col-8"><h4>Members</h4></div>
        <div class="col-4"><a class="btn btn-lg btn-success" href="{{route('lists.members.create', $record->id)}}">Add Member</a></div>
  </div>
  <div class="row">
      <div class="col-4"><strong>Name</strong></div>
      <div class="col-4"><strong>email</strong></div>
  </div>
  @foreach ($record->members as $member)
    <div class="row">
          <div class="col-4">{{$member->firstname}} {{$member->lastname}}</div>
          <div class="col-4">{{$member->email}}</div>
          <div class="col-4">
            <a class="btn btn-primary btn-sm" href="{{route('lists.members.edit', $member->id)}}">Edit</a>
            {!! Form::open([
                'method'=>'DELETE',
                'route' => ['members.destroy', $member->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span>  Delete </span>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'title' => 'Delete Member',
                        'onclick'=>'return confirm("Confirm delete?")'
                )) !!}
            {!! Form::close() !!}

          </div>
    </div>
  @endforeach

</div>
@endsection
