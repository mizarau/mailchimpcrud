@extends('layouts.master')
@section('content')

  <div class="container-fluid id="app"">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>Lists</strong>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      @if ($errors->any())
                      <ul class="alert alert-danger">
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  @endif

                  {!! Form::open(['route' => 'lists.store', 'class' => 'form-horizontal']) !!}

                  @include ('lists.form')

                  {!! Form::close() !!}

                    </div>

                  </div>
                  <!--/.row-->
                </div>
              </div>

            </div>
            <!--/.col-->

@endsection
