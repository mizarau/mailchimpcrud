<div class="form-group">
  {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
  {!! Form::text('name', null, ['class' => 'form-control','placeholder' => 'Enter name']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
   {!! Form::label('user_id', 'Contact:', ['class' => 'control-label']) !!}
   {!! Form::select('user_id',MailChimp\Services\DisplayServices::fetchUser(), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('reminder', 'Permission Reminder', ['class' => 'control-label']) !!}
  {!! Form::text('reminder', 'You are receiving this email because you signed up for it', ['class' => 'form-control','placeholder' => 'Enter a sentence']) !!}
        {!! $errors->first('reminder', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
   {!! Form::label('type_option', 'Let member choose email type:', ['class' => 'control-label']) !!}
   {!! Form::select('type_option',['1' => 'Yes', '0' => 'No'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('from_name', 'From Name', ['class' => 'control-label']) !!}
  {!! Form::text('from_name', null, ['class' => 'form-control','placeholder' => 'Enter from name']) !!}
        {!! $errors->first('from_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
  {!! Form::label('from_email', 'From email', ['class' => 'control-label']) !!}
  {!! Form::text('from_email', null, ['class' => 'form-control','placeholder' => 'Enter from email']) !!}
        {!! $errors->first('from_email', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
  {!! Form::label('subject', 'Subject', ['class' => 'control-label']) !!}
  {!! Form::text('subject', null, ['class' => 'form-control','placeholder' => 'Enter Subject']) !!}
        {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group text-right">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
