@extends('layouts.master')
@section('content')
<div class="starter-template">
  <div class="row">
      <div class="col-9">
            <h1>Lists</h1>
      </div>
      <div class="col-3">
            <a class="btn btn-success" href="{{route('lists.create')}}">New List</a>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
        @if (Session::has('message'))
                 <div class="alert {{Session::get('code')}} alert-dismissable fade show" role="alert">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                   </button>
                   <h4>{{ Session::get('message') }}</h4>
                 </div>
        @endif
      </div>
  </div>
  <div class="row">
      <div class="col-3"><strong>Name</strong></div>
      <div class="col-2"><strong>contact</strong></div>
      <div class="col-3"><strong>subject</strong></div>
      <div class="col-4"></div>
  </div>

  @foreach ($records as $record)
    <div class="row">
        <div class="col-3">{{ $record->name }}</div>
        <div class="col-2">{{ $record->contact->name }}</div>
        <div class="col-3">{{ $record->subject }}</div>
        <div class="col-4">
           <a class="btn btn-warning btn-sm" href="{{route('lists.show', $record->id)}}">Members</a>
           <a class="btn btn-primary btn-sm" href="{{route('lists.edit', $record->id)}}">Edit</a>
           {!! Form::open([
               'method'=>'DELETE',
               'route' => ['lists.destroy', $record->id],
               'style' => 'display:inline'
           ]) !!}
               {!! Form::button('<span>  Delete </span>', array(
                       'type' => 'submit',
                       'class' => 'btn btn-danger btn-sm',
                       'title' => 'Delete List',
                       'onclick'=>'return confirm("Confirm delete?")'
               )) !!}
           {!! Form::close() !!}
        </div>
    </div>
  @endforeach
  {{ $records->links() }}

</div>
@endsection
