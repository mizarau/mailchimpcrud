@extends('layouts.master')
@section('content')

  <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>Edit member - {{$record->email}}</strong>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                          @if ($errors->any())
                              <ul class="alert alert-danger">
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          @endif

                          {!! Form::model($record, [
                              'method' => 'PATCH',
                              'route' => ['members.update', $record->id],
                              'class' => 'form-horizontal',
                              'files' => 'true'
                          ]) !!}
                          @include ('members.form', ['submitButtonText' => 'Update'])
                          {!! Form::close() !!}
                    </div>

                  </div>
                  <!--/.row-->
                </div>
              </div>

            </div>
            <!--/.col-->

@endsection
