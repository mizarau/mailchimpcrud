@extends('layouts.master')
@section('content')
<div class="starter-template">
  <div class="row">
      <div class="col-9">
            <h1>Members</h1>
      </div>
      <div class="col-3">
            <a class="btn btn-success" href="{{route('members.create')}}">New Member</a>
      </div>
  </div>


  <div class="row">
      <div class="col-3"><strong>email</strong></div>
      <div class="col-3"><strong>list</strong></div>
      <div class="col-3"></div>
  </div>

  @foreach ($records as $record)
    <div class="row">
        <div class="col-3">{{ $record->email }}</div>
        <div class="col-3">{{ $record->list->name }}</div>
        <div class="col-3">
           <a class="btn btn-primary btn-sm" href="{{route('members.edit', $record->id)}}">Edit</a>
           {!! Form::open([
               'method'=>'DELETE',
               'route' => ['members.destroy', $record->id],
               'style' => 'display:inline'
           ]) !!}
               {!! Form::button('<span>  Delete </span>', array(
                       'type' => 'submit',
                       'class' => 'btn btn-danger btn-sm',
                       'title' => 'Delete Member',
                       'onclick'=>'return confirm("Confirm delete?")'
               )) !!}
           {!! Form::close() !!}
        </div>
    </div>
  @endforeach
  {{ $records->links() }}

</div>
@endsection
