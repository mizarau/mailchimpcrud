@extends('layouts.master')
@section('content')

  <div class="container-fluid id="app"">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>Add member</strong>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      @if ($errors->any())
                      <ul class="alert alert-danger">
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  @endif
                  {!! Form::open(['route' => array('members.store',$list), 'class' => 'form-horizontal']) !!}

                  @include ('members.form')

                  {!! Form::close() !!}

                    </div>

                  </div>
                  <!--/.row-->
                </div>
              </div>

            </div>
            <!--/.col-->

@endsection
