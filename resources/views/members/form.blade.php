
<div class="form-group">
  {!! Form::label('firstname', 'Firstname', ['class' => 'control-label']) !!}
  {!! Form::text('firstname', null, ['class' => 'form-control','placeholder' => 'Enter firstname', isset($readonly) ? $readonly : null]) !!}
        {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
  {!! Form::label('lastname', 'Lastname', ['class' => 'control-label']) !!}
  {!! Form::text('lastname', null, ['class' => 'form-control','placeholder' => 'Enter lastname',  isset($readonly) ? $readonly : null ]) !!}
        {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
  {!! Form::label('email', 'email', ['class' => 'control-label']) !!}
  {!! Form::text('email', null, ['class' => 'form-control','placeholder' => 'Enter email',  isset($readonly) ? $readonly : null ]) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
   {!! Form::label('status', 'Status:', ['class' => 'control-label']) !!}
   {!! Form::select('status',['subscribed' => 'Subscribed', 'unsubscribed' => 'Unsubscribed', 'cleaned' => 'Cleaned', 'pending' => 'Pending'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group text-right">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
