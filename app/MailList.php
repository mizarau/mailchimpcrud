<?php

namespace MailChimp;

use Illuminate\Database\Eloquent\Model;

class MailList extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * mail list belongs to a contact
     */
    public function contact()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * maillist has members
     */
    public function members()
    {
        return $this->hasMany(Member::class);
    }

}
