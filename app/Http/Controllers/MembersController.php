<?php

namespace MailChimp\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Input;
use MailChimp\Member;
use MailChimp\MailList;
use MailChimp\Services\MailChimp;
use MailChimp\Http\Requests\MembersRequest;


class MembersController extends Controller
{

    /**
     * add member to a list
     */
    public function create($list)
    {
      return view('members.create')->withList($list);
    }

    /**
     * attach a new member to a list
     */
    public function store(MembersRequest $request,  MailList $list)
    {
        $member = new Member([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'status' => $request->status,
        ]);

        $list->members()->save($member);

        $list_id = $list->mailchimp_id;

        $client = new MailChimp(env('MAIL_CHIMP_KEY'));

        $result = $client->post("lists/$list_id/members", [
        				'email_address' => $member->email,
        				'status'        => $member->status,
                'merge_fields' => ['FNAME'=> $member->firstname, 'LNAME'=> $member->lastname],
        			]);


        return Redirect::route('lists.show',$list->id)->with('message', ' Member added')->with('code','alert-success');
    }

    /**
     * edit a member
     */
    public function edit(Member $member)
    {
      if('unsubscribed' == $member->status){
        $readonly = 'readonly';
      } else {
        $readonly = 'false';
      }
      return view('members.edit')->withRecord($member)->withReadonly($readonly);
    }

    /**
     * update the changes of a member
     */
    public function update(MembersRequest $request, Member $member)
    {
        $oldemail = $member->email;
        //dd($oldemail, $request->email);
        $member->firstname = $request->firstname;
        $member->lastname = $request->lastname;
        $member->email = $request->email;
        $member->status = $request->status;
        $member->save();
        $list_id = $member->list->mailchimp_id;
        $client = new MailChimp(env('MAIL_CHIMP_KEY'));
        $subscriber_hash = $client->subscriberHash($oldemail);

        $result = $client->patch("lists/$list_id/members/$subscriber_hash", [
              'email_address' =>  $member->email,
              'status'        =>  $member->status,
              'merge_fields' => ['FNAME'=> $member->firstname, 'LNAME'=> $member->lastname],
			  ]);


        return Redirect::route('lists.show', $member->list->id )->with('message', 'Member updated')->with('code','alert-success');
    }

    /**
     * remove a member from a list
     */
    public function destroy(Member $member)
    {
      $list_id = $member->list->mailchimp_id;
      $client = new MailChimp(env('MAIL_CHIMP_KEY'));
      $subscriber_hash = $client->subscriberHash($member->email);

      $client->delete("lists/$list_id/members/$subscriber_hash");

      $member->delete();

      return redirect::route('lists.index')->with('message','member deleted successfully')->with('code','alert-success');
    }
}
