<?php

namespace MailChimp\Http\Controllers;

use Illuminate\Http\Request;
use MailChimp\MailList;
use Redirect;
use Illuminate\Support\Facades\Input;
use MailChimp\Services\MailChimp;
use MailChimp\Http\Requests\ListsRequest;

class MailListsController extends Controller
{

    /**
     * all the lists
     */
    public function index()
    {
      $records = MailList::paginate(20);
      return view('lists.index')->withRecords($records);
    }

    /**
     * show a specific list
     */
    public function show($list)
    {
      $listWithMembers = MailList::with('members')->where('id',$list)->first();
      return view('lists.show')->withRecord($listWithMembers);
    }

    /**
     * create a mailing list
     */
    public function create()
    {
      return view('lists.create');
    }

    /**
     * store a mailing list
     */
    public function store(ListsRequest $request, MailList $list)
    {
        $list->name = $request->name;
        $list->user_id = $request->user_id;
        $list->permission_reminder = $request->reminder;
        $list->email_type_option = $request->type_option;
        $list->from_name = $request->from_name;
        $list->from_email = $request->from_email;
        $list->subject = $request->subject;
        $list->language = 'en';
        $list->save();

        $client = new MailChimp(env('MAIL_CHIMP_KEY'));


        $httpResponse = $client->post( 'lists',

             [
                 'name' => $request->name,
                 'contact' => [
                   'company' => $list->contact->company,
                   'address1' => $list->contact->address1,
                   'address2' => $list->contact->address2,
                   'city' => $list->contact->city,
                   'state' => $list->contact->state,
                   'zip' => $list->contact->zip,
                   'country' => $list->contact->country,
                   'phone' => $list->contact->phone
                 ],
                 'permission_reminder' => $request->reminder,
                 'campaign_defaults' => [
                     'from_name' => $request->from_name,
                     'from_email' => $request->from_email,
                     'subject' => $request->subject,
                     'language' => 'en'
                 ],
                 'email_type_option' => true
            ]
         );
         //the script will fail if the mailchimp API key is not valid or lack permission to create lists


         $list->mailchimp_id = $httpResponse['id'];
         $list->mailchimp_webid = $httpResponse['web_id'];
         $list->save();

         return Redirect::route('lists.index')->with('message', ' List ' . $list->name . ' added')->with('code','alert-success');
    }

    /**
     * edit an existing list
     */
    public function edit(MailList $list)
    {
      return view('lists.edit')->withRecord($list);
    }

    /**
     * update an existing list
     */
    public function update(ListsRequest $request, MailList $list)
    {
        $list->name = $request->name;
        $list->user_id = $request->user_id;
        $list->permission_reminder = $request->reminder;
        $list->email_type_option = $request->type_option;
        $list->from_name = $request->from_name;
        $list->from_email = $request->from_email;
        $list->subject = $request->subject;
        $list->language = 'en';
        $list->save();

        $client = new MailChimp(env('MAIL_CHIMP_KEY'));
        $list_id = $list->mailchimp_id;

        $httpResponse = $client->patch( 'lists/' .$list_id,

             [
                 'name' => $request->name,
                 'contact' => [
                   'company' => $list->contact->company,
                   'address1' => $list->contact->address1,
                   'address2' => $list->contact->address2,
                   'city' => $list->contact->city,
                   'state' => $list->contact->state,
                   'zip' => $list->contact->zip,
                   'country' => $list->contact->country,
                   'phone' => $list->contact->phone
                 ],
                 'permission_reminder' => $request->reminder,
                 'campaign_defaults' => [
                     'from_name' => $request->from_name,
                     'from_email' => $request->from_email,
                     'subject' => $request->subject,
                     'language' => 'en'
                 ],
                 'email_type_option' => true
            ]
         );

        return Redirect::route('lists.index')->with('message', ' List ' . $list->name . ' updated')->with('code','alert-success');
    }

    /**
     * remove a list
     */
    public function destroy(MailList $list)
    {
        $client = new MailChimp(env('MAIL_CHIMP_KEY'));
        $list_id = $list->mailchimp_id;

        $httpResponse = $client->delete( 'lists/' .$list_id);

        $list->delete();
        return redirect::route('lists.index')->with('message','List deleted successfully')->with('code','alert-success');
    }
}
