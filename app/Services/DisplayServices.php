<?php

namespace MailChimp\Services;

use MailChimp\User;
use MailChimp\MailList;

class DisplayServices
{

    public static function fetchUser()
    {
      return array('' => 'Select contact') + User::pluck('name','id')->all();
    }

    public static function fetchList()
    {
      return array('' => 'Select list') + MailList::pluck('name','id')->all();
    }

    public static function fetchDataCenter()
    {
      list(, $datacentre) = explode('-', env('MAIL_CHIMP_KEY'));
      return $datacentre;
    }

}
