<?php

namespace MailChimp;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * member belongs to a list
     */
    public function list()
    {
        return $this->belongsTo(MailList::class,'mail_list_id');
    }

}
